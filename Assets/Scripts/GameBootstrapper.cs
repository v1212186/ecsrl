﻿using Components;
using Unity.Entities;
using UnityEngine;

public sealed class GameBootstrapper : MonoBehaviour
{

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    static void Initialize()
    {
        CreatePlayer();
    }

    private static void CreatePlayer()
    {
        EntityManager entityManager = World.Active.GetExistingManager<EntityManager>();
        GameObject player = Instantiate(ResourcesManager.Instance.GetEntityPrefab(), Vector3.zero, Quaternion.identity);
        Entity entity = player.GetComponent<GameObjectEntity>().Entity;
        entityManager.AddComponentData(entity, new PlayerInput() { Input = Vector2Int.zero });
        entityManager.AddComponentData(entity, new Position() { position = Vector2Int.zero });
        entityManager.AddSharedComponentData(entity, new CustomAnimation()
        {
            DeltaTime = 0.15f,
            Name = AnimationName.Idle,
            Frames = new Sprite[]
            {
                ResourcesManager.Instance.GetRandomHumanoidSprite()

            }
        });

       player = Instantiate(ResourcesManager.Instance.GetEntityPrefab(), Vector3.zero, Quaternion.identity);
        entity = player.GetComponent<GameObjectEntity>().Entity;
        entityManager.AddSharedComponentData(entity, new CustomAnimation()
        {
            DeltaTime = 0.15f,
            Name = AnimationName.Attack,
            Frames = new Sprite[]
            {
                ResourcesManager.Instance.GetRandomHumanoidSprite()

            }
        });

    }
}
