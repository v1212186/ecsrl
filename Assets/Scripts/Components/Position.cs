﻿using Unity.Entities;
using UnityEngine;

namespace Components
{
    public struct Position : IComponentData
    {
        public Vector2Int position;
    }
}
