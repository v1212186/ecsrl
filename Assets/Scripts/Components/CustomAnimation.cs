﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Components
{
    public enum AnimationName
    {
        Idle, Attack, Hit, Walk
    }

    public struct CustomAnimation : ISharedComponentData
    {
        #region Variables

        public AnimationName Name;
        public float DeltaTime;
        public Sprite[] Frames;

        #endregion
    }
}
