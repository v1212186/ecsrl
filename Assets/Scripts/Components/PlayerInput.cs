﻿using Unity.Entities;
using UnityEngine;

namespace Components
{
    public struct PlayerInput : IComponentData
    {

        public Vector2Int Input;

    }
}
