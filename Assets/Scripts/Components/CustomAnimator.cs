﻿//using System.Collections;
//using System.Collections.Generic;
//using Unity.Entities;
//using UnityEngine;

//namespace Components
//{
//    public class CustomAnimator : IComponentData
//    {
//        #region Variables
//        [SerializeField]
//        [Range(0, 1)]
//        private float animationSpeedMultiplier = 1.0f;
//        [SerializeField]
//        private AnimationCurve movementAnimationCurve;
//        [SerializeField]
//        private List<CustomAnimation> animations;
//        public List<CustomAnimation> Animations
//        {
//            get
//            {
//                if (animations == null)
//                {
//                    animations = new List<CustomAnimation>();
//                }
//                return animations;
//            }
//        }
//        [SerializeField]
//        private int currentAnimation = 0;
//        private int currentFrame;
//        private int CurrentFrame
//        {
//            set
//            {
//                currentFrame = value;
//                SetAnimationFrame();
//            }
//            get
//            {
//                return currentFrame;
//            }
//        }
//        private float timer;
//        private Dictionary<AnimationName, int> names = new Dictionary<AnimationName, int>();
//        public SpriteRenderer SpriteRenderer { get { return Entity.SpriteRenderer; } }
//        #endregion

//        #region Monobehaviour methods
//        void Update()
//        {
//            timer -= Time.deltaTime;
//            if (timer < 0f)
//            {
//                timer += animations[currentAnimation].DeltaTime * animationSpeedMultiplier;
//                currentFrame++;
//                if (currentFrame >= animations[currentAnimation].Frames.Length)
//                    currentFrame = 0;
//                SetAnimationFrame();
//            }
//        }
//        #endregion

//        #region Methods

//        public void Initialize(CustomAnimation[] _animation, AnimationCurve _animationCurve, float _animationSpeedMultiplier)
//        {
//            if (_animation == null)
//            {
//                return;
//            }
//            for (int i = 0; i < _animation.Length; i++)
//            {
//                Animations.Add(_animation[i]);
//            }
//            RefillDictionary();
//            movementAnimationCurve = _animationCurve;
//            animationSpeedMultiplier = _animationSpeedMultiplier;
//        }

//        private void RefillDictionary()
//        {
//            names.Clear();
//            for (int i = 0; i < animations.Count; i++)
//            {
//                names.Add(animations[i].Name, i);
//            }
//        }

//        private void SetAnimationFrame()
//        {
//            Sprite animationFrame = animations[currentAnimation].Frames[currentFrame];
//            SpriteRenderer.sprite = animationFrame;
//        }

//        private void SetAnimation(int _num)
//        {
//            currentAnimation = _num;
//            CurrentFrame = 0;
//            SetAnimationFrame();
//        }

//        public void SetAnimation(AnimationName _name)
//        {
//            if (names.ContainsKey(_name))
//            {
//                SetAnimation(names[_name]);
//            }
//        }

//        public int GetCurrentAnimationLength()
//        {
//            return animations[currentAnimation].Frames.Length;
//        }

//        public float GetCurrentAnimationLengthInSeconds()
//        {
//            return animations[currentAnimation].Frames.Length * animations[currentAnimation].DeltaTime * animationSpeedMultiplier;
//        }

//        private void MirrorSprite(bool _facingRight)
//        {
//            SpriteRenderer.flipX = _facingRight;
//        }

//        //By default sprites are facing left side
//        //TODO переписать не через дирекшин, а через точку которую выбрали
//        public void MirrorSprite(Vector2Int _direction)
//        {
//            if (_direction.x < 0)
//            {
//                MirrorSprite(false);
//            }
//            if (_direction.x > 0)
//            {
//                MirrorSprite(true);
//            }
//        }

//        public IEnumerator PlayAnimation(AnimationName _animationName)
//        {
//            SetAnimation(_animationName);
//            float animTime = GetCurrentAnimationLengthInSeconds();
//            float animationTimer = 0.0f;
//            while (animationTimer <= animTime)
//            {
//                animationTimer += Time.deltaTime;
//                yield return new WaitForEndOfFrame();
//            }
//            ResetToIdleAnimation();
//        }

//        public IEnumerator PlayAnimationAndMoveActor(AnimationName _animationName, Vector2Int _position)
//        {
//            SetAnimation(_animationName);
//            float animTime = GetCurrentAnimationLengthInSeconds();
//            Vector2 start = Entity.Position.GetVector2();
//            Vector2 end = _position.GetVector2();
//            float movementTimer = 0.0f;
//            Entity.UpdatePosition(_position);
//            while (movementTimer <= animTime)
//            {
//                transform.localPosition = Vector2.Lerp(start, end, movementAnimationCurve.Evaluate(movementTimer / animTime));
//                movementTimer += Time.deltaTime;
//                yield return new WaitForEndOfFrame();
//            }
//            transform.localPosition = end;
//            ResetToIdleAnimation();
//        }

//        public void ResetToIdleAnimation()
//        {
//            SetAnimation(AnimationName.Idle);
//        }
//        #endregion
//    }
//}
