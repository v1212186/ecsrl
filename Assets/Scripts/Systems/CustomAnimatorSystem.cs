﻿using Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Rendering;
using UnityEngine;

namespace Systems
{
    public class CustomAnimatorSystem : ComponentSystem {

        public struct Data
        {
            public int Length;
           [ReadOnly] public SharedComponentDataArray<CustomAnimation> CustomAnimation;
        }

        [Inject]
        private Data data;

        protected override void OnUpdate()
        {
            for (int i = 0; i < data.Length; i++)
            {
                Debug.Log(data.CustomAnimation[i].Name);
            }
        }
    }
}
