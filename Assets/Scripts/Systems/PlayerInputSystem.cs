﻿using Components;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Systems
{
    public class PlayerInputSystem : ComponentSystem
    {

        public struct Data
        {
            public int Length;
            public GameObjectArray GameObject;
            public ComponentDataArray<PlayerInput> PlayerInput;
        }

        [Inject] private Data data;

        protected override void OnUpdate()
        {
            for (int i = 0; i < data.Length; i++)
            {
                int h = 0, v = 0;

                if (Input.GetKeyDown(KeyCode.A))
                {
                    h = -1;
                }
                if (Input.GetKeyDown(KeyCode.W))
                {
                    v = 1;
                }
                if (Input.GetKeyDown(KeyCode.D))
                {
                    h = 1;
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    v = -1;
                }


                Vector2Int newInput = new Vector2Int(h, v);

                data.PlayerInput[i] = new PlayerInput() { Input = newInput };
            }
        }

        protected override void OnCreateManager(int capacity)
        {
            base.OnCreateManager(capacity);
        }
    }
}
