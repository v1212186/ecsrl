﻿using Components;
using Unity.Entities;
using UnityEngine;

namespace Systems
{
    public class MovementSystem : ComponentSystem
    {

        public struct Data
        {
            public int Length;
            public GameObjectArray GameObject;
            public ComponentDataArray<PlayerInput> PlayerInput;
            public ComponentDataArray<Position> Position;
        }

        [Inject] private Data data;

        protected override void OnUpdate()
        {

            for (int i = 0; i < data.Length; i++)
            {

                Vector2Int currentPosition = data.Position[i].position;

                currentPosition += data.PlayerInput[i].Input;

                data.Position[i] = new Position { position = currentPosition };

                data.GameObject[i].transform.position = new Vector3(data.Position[i].position.x, data.Position[i].position.y, 0.0f);
            }
        }
    }
}
