﻿using UnityEngine;

public class ResourcesManager : MonoBehaviour
{

    #region Variables

    private static ResourcesManager instance;
    public static ResourcesManager Instance
    {
        get { return instance; }
    }

    [SerializeField]
    private GameObject entityPrefab;

    [SerializeField]
    private Sprite[] humanoid;

    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
    }

    public Sprite GetRandomHumanoidSprite()
    {
        return humanoid[Random.Range(0, humanoid.Length)];
    }

    public GameObject GetEntityPrefab()
    {
        return entityPrefab;
    }

    #endregion

}
